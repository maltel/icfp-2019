# Intro

This README file was written under some time pressure. If any clarifications
are needed, you are welcome to contact me under my email address malte@leip.net

# Programming language and requirements

The program consists only of the file `solver.py`.
It is written in Python 3, and was developed with Python 3.5.3, though later
versions should also work. It also needs a couple of standard Python libraries,
a precise list can be obtained from the first couple of lines of the source
code.


# Steps to generate solutions:

## 1.

In order to run it, one should first customize some hardcoded paths in the
file, from line 66 to 70. These are:
base_path_problems: a folder in which the problem files are expected to
                    be located (and nothing more).
base_path_solutions: an empty folder in which the solutions are going to be
                     written (don't change anything in here except deleting
                     file)
base_path_submission: folder in which all the submission archives will be stored
                      (again, don't add anything else here)
base_path_tmp:        path to a folder that should *not* exist, but that can
                      be created by the program
path_manager_configuration: file that the manager uses to store/load configuration
                            data.

Problem files are expected to have filename "prob-XYZ.desc", where XYZ is a three-digit
number (possibly with leading zeros).
A solution created for such a problem will be stored in:
base_path_solutions/prob-XYZ/STEPS-ID
where STEPS is the number of steps that the solution takes and ID is a five-digit number
(padded with zeros).

## 2.

Create an initial manager configuration at path_manager_configuration:

{"team_name": "TEAMNAME", "api_id": "APIID", "last_solution_submission_timestamp": 0, "total_steps_submitted": 0, "solution_archives": {}, "newly_solved_problems": false, "total_steps": 0, "last_submission_best_team_steps": {}}

Of course, replace TEAMNAME by the team name, and APIID by the API id.

## 3.

To solve a single problem, for example problem 42, you can run:

python3 -O solver.py solve 42 15

in this case, 15 is the time budget. It is an optional parameter that is used
by the manager and solver to allocate time for each step. As it is no known beforehand
how many steps a problem takes this should not be interpreted as "15 seconds", but only
as an abstract number [it did originate with the "seconds" meaning though]. basically,
if you want to give the solver more time, increase the number. this does not necessarly
make the solution better, sometimes it even gets worse.

to automatically run the solver you can use:

python3 -O solver.py auto 2

the number is in this case the number of parallel processes that should be spawned.
It will manage everything, including automatically uploading solutions. As the server
does not accept solutions anymore, that unfortunately means that this will throw
an error soon.

python3 -O solver.py info

will output some information, such as the total number of steps needed for all
the best solutions, and the total number currently submitted.

python3 -O solver.py stats

shows you the best solver for each problem and how many steps it took

python3 -O solver.py check-counts

was used to check that the uploads were accepted and that the numbers of steps
given by the server do not deviate from our expectation.

Finally,
python3 -O solver.py visualize PROBLEM SOLUTION
takes a problem file and a solution file (our format, not the contests, i.e.
the files that are outputted by the solver) and draws a map. to advance
one step one needs to press enter.
For the larger problems this takes very long however. This is why the "solve"
command only draws the map ever couple of seconds (depending on the size of
the problem)


# Solution approach

For the lightning division, the algorithm ignored all boosters and just
calculated the shortest path to the next empty tile (it did not move all
the way there in one step though, but only up to a distance given by
the radius of the manipulators around it, in order to save the extra one
or two steps if it is not necessary for the bot itself to move to the tile).

Later I implemented the booster actions from the original specification.
The new solver builds up chains of deltas to the current situation (with
some crude method of discarding definitely uninteresting ones given those
we already have). After the time is elapsed, the best delta is chosen
by a formula that values reducing the boundary length of the empty (not
obstruction and not wrapped) space, holding boosters, having active fast
wheels, reducing the amount of empty tiles while using as little steps as
possible, etc. [see the evaluate_delta function for this]
The precise weights are hand-adjusted via some test runs. I did not test
them a lot though. My original plan was to have them be adjusted by the
manager automatically to find the optimal solver, but I (and my computer,
as recalculating solutions with the latest solver versions always took quite
a while) did not have time for that.


# Brief overview over the code:

function manipulator_vectors_for_number_direction:
    how manipulators are attached is static and done in the form of a fan
    (as the other spots around the bot can then be reached by turning).
    this function calculates where the manipulators should be.

class RichSolution: used to save some extra information in our own solution
    file format (a magic line at the top and then the data held in the class
    in json format). Basically so we can keep track of which solver (and
    solver version produced the solution and how long it was ago, so the
    manager can prioritize re-running problems whose solution is oldest.

class Point: basically just x and y coordinates plus some helpers

class Line: used for horizontal or vertical lines between two points,
    holding them in a normalized way. used for calculating intersections
    between two such lines, which is implemented in this class

class PolygonChain: a sequence of points that make up a (closed or not) polygon.
    I originally tested whether a line segment was part of the polygon by
    creating the maximally finest representation of the polygon and then checking
    wether the path was a sublist in it. this was not very fast though, so I
    changed to calculating line intersections. Using this, this class can
    calculate which tiles are inside a polygon.

class Situation: Holds the entire situation we are in, with the map, where the bot
    is, what boosters are held by it, etc. This class implements loading problem
    files, printing a nice graphical representation of the situation, etc.

class SituationDelta: because copying an entire Situation every time we want to
    see what would happen, there exists this more light-weight class for storing
    deltas to a Situation. The two classes are not very well separated though, as
    I only started implementing SituationDelta at a later time and only moved things
    as necessary, leading to some deduplication.

function is_delta_better: returns True only if the first delta is clearly
    better than the second one. used in the solver to throw away uninteresting deltas

function calculate_empty_side_score_situation: calculates the boundary length of
    the empty tiles (times -1). so an isolated empty tile gets -4, two neighboring
    but apart from that isolated ones get each -3, etc.

function calculate_empty_side_score_delta:
    calculate the change to the above function for a delta

function evaluate_delta: give a delta a score. explained above.

function best_delta: finds the best delta, which usually means the one with the best score,
    but if we are actually close to solving the problem, then we don't care and just
    take the quickest solution.

function delta_solver1: finds a delta to apply next by iteratively constructing chains
    of deltas until the time runs out or we found a way to reduce the empty count by
    at least one, whatever takes longer.

function path_solverN: older solvers that mostly just looked for the shortest path
    to any empty tile and took that. probably can't be run anymore.

class Solver: wrapper for running solvers, keeping track of time etc.

class ProblemSolutionInfo: used by Manager to hold information about solutions in memory
    not everything is needed actually, for example we don't need the actual solution command
    strings, but I did not get around to making it more efficient

class Manager: handles everything: can run one solvers, tell you statistics, automatically
    decide what problems should be attempted next and spawn processes to do so, creates
    submission archives, uploads them, and verifies that the answers from the server are
    as expected.



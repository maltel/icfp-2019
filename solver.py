#!/usr/bin/python3

# Copyright (C) 2019  Malte Leip <malte@leip.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import pathlib
import time
import shutil
import os
import json
import requests
#import cProfile
#import tracemalloc
import copy
import hashlib
import math
import multiprocessing
import threading

TILE_EMPTY = '-'
TILE_OBSTACLE = 'O'
TILE_DONE = 'P'
DIRECTION_UP = 'W'
DIRECTION_DOWN = 'S'
DIRECTION_LEFT = 'A'
DIRECTION_RIGHT = 'D'
DIRECTION_IRRELEVANT = 'i'
DIRECTIONS=[DIRECTION_UP, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT]
BOOSTER_MANIPULATOR = 'B'
BOOSTER_FAST_WHEELS = 'F'
BOOSTER_DRILL = 'L'
BOOSTER_MYSTERIOUS = 'X'
BOOSTER_TELEPORTER = 'R'
BOOSTER_CLONING = 'C'
BOOSTER_TYPES = [BOOSTER_MANIPULATOR, BOOSTER_FAST_WHEELS,
        BOOSTER_DRILL, BOOSTER_MYSTERIOUS, BOOSTER_TELEPORTER,
        BOOSTER_CLONING]
TURN_CLOCKWISE = 'E'
TURN_COUNTER_CLOCKWISE = 'Q'
TURNS = [TURN_CLOCKWISE, TURN_COUNTER_CLOCKWISE]
COMMAND_SHIFT = 'T'

COMMANDS = copy.deepcopy(DIRECTIONS)
COMMANDS.extend(TURNS)
COMMANDS.extend([BOOSTER_FAST_WHEELS, BOOSTER_DRILL, BOOSTER_MANIPULATOR])

BACKGROUND_COLOR_DEFAULT='\033[49m'
BACKGROUND_COLOR_BLACK='\033[40m'
BACKGROUND_COLOR_RED='\033[41m'
BACKGROUND_COLOR_GREEN='\033[42m'
BACKGROUND_COLOR_YELLOW='\033[43m'
BACKGROUND_COLOR_BLUE='\033[44m'
BACKGROUND_COLOR_MAGENTA='\033[45m'
BACKGROUND_COLOR_CYAN='\033[46m'
BACKGROUND_COLOR_GRAY='\033[47m'

FOREGROUND_COLOR_DEFAULT='\033[39m'
FOREGROUND_COLOR_BLACK='\033[30m'
FOREGROUND_COLOR_RED='\033[31m'
FOREGROUND_COLOR_GREEN='\033[32m'
FOREGROUND_COLOR_YELLOW='\033[33m'
FOREGROUND_COLOR_BLUE='\033[34m'
FOREGROUND_COLOR_MAGENTA='\033[35m'
FOREGROUND_COLOR_CYAN='\033[36m'
FOREGROUND_COLOR_GRAY='\033[37m'


# Customize this!
base_path_problems    = '/home/user/readme_test/problems/'
base_path_solutions   = '/home/user/readme_test/solutions/'
base_path_submissions = '/home/user/readme_test/submissions/'
base_path_tmp         = '/home/user/readme_test/auto_tmp/'
path_manager_configuration = '/home/user/readme_test/manager.conf'

time_last_print = time.perf_counter() - 100

current_rich_solution_version = 1
delta_solver_version = 12

class IllegalCommandError(Exception):
    pass

def turn_direction(to_turn, direction):
    if direction == TURN_CLOCKWISE:
        if to_turn == DIRECTION_LEFT:
            return DIRECTION_UP
        elif to_turn == DIRECTION_UP:
            return DIRECTION_RIGHT
        elif to_turn == DIRECTION_RIGHT:
            return DIRECTION_DOWN
        else:
            return DIRECTION_LEFT
    else:
        if to_turn == DIRECTION_LEFT:
            return DIRECTION_DOWN
        elif to_turn == DIRECTION_UP:
            return DIRECTION_LEFT
        elif to_turn == DIRECTION_RIGHT:
            return DIRECTION_UP
        else:
            return DIRECTION_RIGHT
    re

def manipulator_vectors_for_number(number):
    ret = {}
    for direction in DIRECTIONS:
        ret[direction] = manipulator_vectors_for_number_direction(number, direction)
    return ret


def manipulator_vectors_for_number_direction(number, direction):
    ret = []
    center_vector = Point(0,0)
    row = 0
    forwards_direction = direction
    left_direction = turn_direction(forwards_direction, TURN_COUNTER_CLOCKWISE)
    right_direction = turn_direction(forwards_direction, TURN_CLOCKWISE)
    while len(ret) < number:
        row += 1
        center_vector = center_vector.translate(forwards_direction)
        vector = copy.deepcopy(center_vector)
        for i in range(0,row + 1):
            ret.append(vector)
            vector = vector.translate(left_direction)
            if len(ret) >= number:
                break
        vector = copy.deepcopy(center_vector)
        for i in range(0,row):
            if len(ret) >= number:
                break
            vector = vector.translate(right_direction)
            ret.append(vector)
    return ret

def full_range(x,y):
    if x < y:
        return range(x,y+1)
    else:
        return range(y,x+1,)

class RichSolution():
    def __init__(self, **kwargs):
            #solution=None, steps=None, solution_id=None, timestamp=None, runtime=None,\
            #solver=None, solver_version=None, problem_id=None):
        #self.solution = solution
        #self.steps = steps
        #self.solution_id = solution_id
        #self.timestamp = timestamp
        #self.runtime = runtime
        #self.solver = solver
        #self.solver_version = solver_version
        #self.problem_id = problem_id
        self.on_disk = False
        self.__dict__.update(kwargs)

    def solution_path_dir(self):
        path_solution_dir = pathlib.Path(base_path_solutions + 'prob-' + str(self.problem_id).zfill(3))
        path_solution_dir.mkdir(exist_ok=True)
        if not path_solution_dir.is_dir():
            raise Exception("Directiory {} does not exist and could not be created!".format(path_solution_dir))
        return path_solution_dir


    def find_solution_id(self):
        path_solution_dir=self.solution_path_dir()
        solution_id = 0
        while True:
            solution_id_str = str(solution_id).zfill(5)
            filename = str(self.steps) + '-' + solution_id_str
            solution_path = path_solution_dir / filename
            if solution_path.exists():
                existing_solution = RichSolution()
                existing_solution.read_from_disk(solution_path)
                if (existing_solution.solution == self.solution) and\
                        (existing_solution.solver == self.solver) and\
                        (existing_solution.solver_version == self.solver_version):
                    #print("Solution is identical with {}".format(filename))
                    self.solution_id = solution_id
                    self.on_disk = True
                    return
            else:
                self.solution_id = solution_id
                return
            solution_id = solution_id + 1

    def solution_output_string(self):
        ret = '!udfewer #001\n'
        ret = ret + json.dumps(vars(self))
        return ret

    def path(self):
        return self.solution_path_dir() / (str(self.steps) + '-' + str(self.solution_id).zfill(5))

    def write_to_disk(self, overwrite=False):
        self.find_solution_id()
        if self.on_disk and not overwrite:
            print("No need to write, already on disk with id {}".format(self.solution_id))
            # FIXME: check this here
            return
        self.on_disk = True
        path = self.path()
        #print("Will write to file {}".format(path))
        content = self.solution_output_string()
        #print("Content:\n{}".format(content))
        with path.open('w') as f:
            f.write(content)

    def read_from_disk(self, path):
        with path.open() as f:
            version_line = f.readline()
            if version_line.startswith("!udfewer #"):
                version = int(version_line.split(sep='#')[1])
            else:
                version = 0
            #print("Reading a solution file in version {}".format(version))
            rest = f.read()

        self.on_disk = True
        self.steps = int(path.name.split(sep='-')[0])
        self.solution_id = int(path.name.split(sep='-')[1])

        if version == 0:
            # old version, just the solution
            self.solution = version_line
            self.timestamp = int(path.stat().st_mtime)
            self.solver = 'path'
            self.solver_version = 0
            self.problem_id = int(path.parent.name.split(sep='-')[1])

        if version == 1:
            self.__dict__.update(json.loads(rest))

        self.steps = int(self.steps)
        self.on_disk = True
        self.solver_version = int(self.solver_version)

    def upgrade_on_disk(self):
        path = self.path()
        print("Upgrading {}".format(path))
        self.write_to_disk(overwrite=True)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return 'RichSolution ' + str(self.__dict__)

    def spec_filename(self):
        return 'prob-' + str(self.problem_id).zfill(3) + '.sol'


class Point:
    def __init__(self, x=None, y=None, from_iterable=None, from_string=None):
        if from_iterable:
            self.x = from_iterable[0]
            self.y = from_iterable[1]
        elif from_string:
            self.__init__(from_iterable=[int(y) for y in from_string[1:-1].split(sep=',')])
        else:
            self.x = x
            self.y = y

    def __str__(self):
        return '({},{})'.format(self.x,self.y)

    def __repr__(self):
        return 'Point{}'.format(self.__str__())

    def __eq__(self,other):
        return self.x == other.x and self.y == other.y

    def __ne__(self,other):
        return self.x != other.x or self.y != other.y

    def __hash__(self):
        return hash((self.x,self.y))

    def translate(self, direction):
        if direction == DIRECTION_UP:
            return Point(self.x,self.y + 1)
        elif direction == DIRECTION_DOWN:
            return Point(self.x,self.y - 1)
        elif direction == DIRECTION_LEFT:
            return Point(self.x - 1,self.y)
        elif direction == DIRECTION_RIGHT:
            return Point(self.x + 1,self.y)
        else:
            raise ValueError('Wrong direction: {}'.format(direction))

    def __add__(self, other):
        return(Point(self.x + other.x, self.y + other.y))

# This assumes that the lines are either horizontal
# or vertical
class Line:
    def __init__(self, point_1, point_2):
        if point_1.x == point_2.x:
            self.orientation = 'v'
            if point_1.y < point_2.y:
                self.point_1 = point_1
                self.point_2 = point_2
            elif point_1.y > point_2.y:
                self.point_2 = point_1
                self.point_1 = point_2
            else:
                raise Exception
        elif point_1.y == point_2.y:
            self.orientation = 'h'
            if point_1.x < point_2.x:
                self.point_1 = point_1
                self.point_2 = point_2
            elif point_1.x > point_2.x:
                self.point_2 = point_1
                self.point_1 = point_2
            else:
                raise Exception
        else:
            raise Exception

    def __str__(self):
        return str((self.point_1, self.point_2))

    def __repr__(self):
        return 'Line({})'.format(self.__str__())

    def intersect(self,other):
        #print("Checking whether {} and {} intersect".format(self,other))
        if self.orientation == other.orientation:
            return False
        if self.orientation == 'h':
            if (self.point_1.y < other.point_2.y) and \
                    (self.point_1.y > other.point_1.y) and \
                    (self.point_1.x < other.point_1.x) and \
                    (self.point_2.x > other.point_1.x):
                return True
        else:
            if (self.point_1.x < other.point_2.x) and \
                    (self.point_1.x > other.point_1.x) and \
                    (self.point_1.y < other.point_1.y) and \
                    (self.point_2.y > other.point_1.y):
                return True
        return False


class PolygonChain:
    def __init__(self, chain, closed=False, from_string=False):
        if from_string:
            # Remove the last ')'
            chain = chain[0:-1]
            chain = chain.split(sep='),')
            # Now we have a list of strings of the form "('n','m'".
            chain = [x[1:].split(sep=',') for x in chain]
            # Now we have a list of lists: ['n','m']
            self.chain = [Point(from_iterable=tuple([int(y) for y in x])) for x in chain]

        else:
            self.chain = chain
        self.closed = closed

    def __str__(self):
        return self.chain.__str__()

    # Maximal way to describe the polygonal chain
    def _generate_max_chain(self):
        if hasattr(self,'max_chain'):
            return
        def add_from_to(start, end):
            if start.x == end.x:
                if start.y < end.y:
                    ys = list(range(start.y + 1, end.y + 1))
                else:
                    ys = list(range(start.y - 1,end.y - 1,-1))
                xs = [start.x] * len(ys)
            else:
                if start.x < end.x:
                    xs = list(range(start.x + 1, end.x + 1))
                else:
                    xs = list(range(start.x - 1,end.x - 1,-1))
                ys = [start.y] * len(xs)
            ret = list(zip(xs,ys))
            ret = [Point(from_iterable=xy) for xy in ret]
            return ret
        current_start = self.chain[0]
        self.max_chain = [current_start]
        for current_end in self.chain[1:]:
            self.max_chain.extend(add_from_to(current_start,current_end))
            current_start = current_end
        if self.closed:
            self.max_chain.extend(add_from_to(current_start,self.chain[0]))

    def has_subchain(self, subchain):
        self._generate_max_chain()
        subchain._generate_max_chain()
        for i in range(0, len(self.max_chain)):
            #FIXME: circular checking, i.e. go over the end to the beginning if
            # big chain is closed.
            for j in range(0, min(len(subchain.max_chain), len(self.max_chain)-i)):
                #print('Testing i={} j={}'.format(i,j))
                if self.max_chain[i+j] != subchain.max_chain[j]:
                    break
            if self.max_chain[i+j] == subchain.max_chain[j] \
                    and j == len(subchain.max_chain) - 1:
                return True
        return False

    def _find_min_max(self):
        if hasattr(self,'min_x') and hasattr(self,'min_y') and \
                hasattr(self,'max_x') and hasattr(self,'max_y'):
            return
        self.max_x = max([point.x for point in self.chain])
        self.max_y = max([point.y for point in self.chain])
        self.min_x = min([point.x for point in self.chain])
        self.min_y = min([point.y for point in self.chain])


    def tiles_inside_1(self):
        self._find_min_max()
        ret = []
        for x in range(self.min_x - 1, self.max_x):
            inside = False
            for y in range(self.min_y, self.max_y):
                if self.has_subchain(PolygonChain([Point(x,y),Point(x + 1,y)])) \
                        or self.has_subchain(PolygonChain([Point(x + 1,y),Point(x,y)])):
                    inside = not inside
                if inside:
                    ret.append(Point(x,y))
        return ret

    def _generate_horizontal_vertical(self):
        if hasattr(self, '_horizontal'):
            return
        self._horizontal = []
        self._vertical = []
        for i in range(0,len(self.chain)):
            if i + 1 < len(self.chain):
                line = Line(self.chain[i], self.chain[i+1])
            elif self.closed:
                line = Line(self.chain[i], self.chain[0])
            if line.orientation == 'h':
                self._horizontal.append(line)
            else:
                self._vertical.append(line)

    def intersect(self,line):
        if line.orientation == 'h':
            lines = self._vertical
        else:
            lines = self._horizontal
        return not all([(not line.intersect(l)) for l in lines])

    def tiles_inside(self):
        return self.tiles_inside_3()

    def tiles_inside_2(self):
        self._generate_horizontal_vertical()
        self._find_min_max()
        ret = []
        #print(self.chain)
        #print(self._horizontal)
        #print(self._vertical)
        for x in range(self.min_x - 1, self.max_x):
            inside = False
            for y in range(self.min_y, self.max_y):
                line = Line(Point(x+(1/2),y-(1/2)),Point(x+(1/2),y+(1/2)))
                #print(line)
                if self.intersect(line):
                    #print("Intersection!")
                    inside = not inside
                if inside:
                    ret.append(Point(x,y))
        return ret

    def _generate_intersection_hash(self):
        if hasattr(self,'_intersection_hash'):
            return
        self._generate_horizontal_vertical()
        self._intersection_hash = {}
        for line in self._horizontal:
            y = line.point_1.y
            for x in range(line.point_1.x,line.point_2.x):
                point = Point(x,y)
                self._intersection_hash[point] = 1

    def tiles_inside_3(self):
        self._generate_intersection_hash()
        self._find_min_max()
        ret = []
        #print(self.chain)
        #print(self._horizontal)
        #print(self._vertical)
        for x in range(self.min_x - 1, self.max_x):
            inside = False
            for y in range(self.min_y, self.max_y):
                if Point(x,y) in self._intersection_hash:
                    #print("Intersection!")
                    inside = not inside
                if inside:
                    ret.append(Point(x,y))
        return ret

class SituationDelta:
    def __init__(self, parent_situation = None, command=None, save_commands = True, from_delta = None, command_extra=None):
        self.parent_situation = parent_situation
        self._map_tiles_delta = {}
        self._booster_locations_deleted = []
        self._boosters_held_change = {}
        self._command_sequence_addition = []
        self._set_up_teleporter_locations_addition = []
        self.save_commands = save_commands
        if not from_delta is None:
            delta_dict = from_delta.__dict__
            for key in delta_dict:
                if key == 'parent_situation':
                    setattr(self, key, delta_dict[key])
                else:
                    setattr(self, key, copy.deepcopy(delta_dict[key]))
        if not command is None:
            self.apply_command(command,command_extra)

    def __eq__(self, other):
        dict1 = self.__dict__
        dict2 = other.__dict__
        # Fixme: some deltas should be considered same even with
        # different keys...
        #for key in set(dict1.keys()).union(dict2.keys()):
            #print("Checking key {}".format(key))
        if set(dict1.keys()) != set(dict2.keys()):
            return False
        for key in dict1.keys():
            if dict1[key] != dict2[key]:
                return False
        return True

    #TODO: finish this.
    def __key(self):
        return (
                id(self.parent_situation),
                frozenset(self._map_tiles_delta.items()),
                self._booster_locations_deleted,
                self._boosters_held_change,
                self._command_sequence_addition,
                self.save_commands,
                self.bot_location,
                self.nanipulator_vectors,
                self.orientation,
                self.empty,
                self.steps,
                self.steps_wheels_remaining
                )

    def __hash__(self):
        return(hash(self.__key()))

    def get_attribute(self, name):
        #print("Getting SituationDelta attribute {}".format(name))
        if hasattr(self, '_' + name):
            return getattr(self, '_' + name)
        #print("Getting it from parent")
        return getattr(self.parent_situation, name)

    def del_attribute(self, name):
        if hasattr(self, '_' + name):
            delattr(self,'_' + name)

    @property
    def bot_location(self):
        return self.get_attribute('bot_location')
    @bot_location.setter
    def bot_location(self, value):
        self._bot_location = value
    @bot_location.deleter
    def bot_location(self):
        self.del_attribute('_bot_location')

    def manipulator_vectors(self, direction):
        if hasattr(self, '_manipulator_vectors'):
            return self._manipulator_vectors[direction]
        return self.parent_situation.manipulator_vectors[direction]

    def set_manipulator_vectors(self, vectors):
        self._manipulator_vectors = vectors

    @property
    def orientation(self):
        return self.get_attribute('orientation')
    @orientation.setter
    def orientation(self, value):
        self._orientation = value
    @orientation.deleter
    def orientation(self):
        self.del_attribute('_orientation')

    @property
    def empty(self):
        return self.get_attribute('empty')
    @empty.setter
    def empty(self, value):
        self._empty = value
    @empty.deleter
    def empty(self):
        self.del_attribute('_empty')

    @property
    def steps(self):
        return self.get_attribute('steps')
    @steps.setter
    def steps(self, value):
        self._steps = value
    @steps.deleter
    def steps(self):
        self.del_attribute('_steps')

    @property
    def steps_wheels_remaining(self):
        return self.get_attribute('steps_wheels_remaining')
    @steps_wheels_remaining.setter
    def steps_wheels_remaining(self, value):
        self._steps_wheels_remaining = value
    @steps_wheels_remaining.deleter
    def steps_wheels_remaining(self):
        self.del_attribute('_steps_wheels_remaining')

    @property
    def steps_drill_remaining(self):
        return self.get_attribute('steps_drill_remaining')
    @steps_drill_remaining.setter
    def steps_drill_remaining(self, value):
        self._steps_drill_remaining = value
    @steps_drill_remaining.deleter
    def steps_drill_remaining(self):
        self.del_attribute('_steps_drill_remaining')

    def get_command_sequence(self):
        return self.parent_situation.command_sequence + self._command_sequence_addition

    def append_command_to_sequence(self, command):
        self._command_sequence_addition.append(command)

    def get_set_up_teleporter_locations(self):
        return self.parent_situation.set_up_teleporter_locations + self._set_up_teleporter_locations_addition
    
    def append_set_up_teleporter_locations(self, location):
        self._set_up_teleporter_locations_addition.append(location)

    @property
    def max_x(self):
        return self.get_attribute('max_x')

    @property
    def max_y(self):
        return self.get_attribute('max_y')

    @property
    def teleporter_divisions(self):
        return self.get_attribute('teleporter_divisions')

    def get_tile(self, point):
        if point in self._map_tiles_delta:
            return self._map_tiles_delta[point]
        return self.parent_situation.map_tiles[point.x][point.y]

    def set_tile(self, point, value):
        self._map_tiles_delta[point] = value

    def get_boosters(self):
        boosters = dict(self.parent_situation.booster_type)
        for to_delete in self._booster_locations_deleted:
            del boosters[to_delete]
        return boosters

    def del_booster(self, location):
        self._booster_locations_deleted.append(location)

    def get_boosters_held(self, booster_type):
        if booster_type in self._boosters_held_change:
            return self.parent_situation.boosters_held[booster_type] + \
                    self._boosters_held_change[booster_type]
        return self.parent_situation.boosters_held[booster_type]

    def boosters_held_add(self, booster_type, change):
        if booster_type not in self._boosters_held_change:
            self._boosters_held_change[booster_type] = change
        else:
            self._boosters_held_change[booster_type] += change
        if self._boosters_held_change[booster_type] == 0:
            del self._boosters_held_change[booster_type]

    def apply_command(self, command, command_extra = None):
        if command in DIRECTIONS:
            self.apply_command_move(command)
        elif command in TURNS:
            self.apply_command_turn(command)
        elif command in BOOSTER_TYPES:
            command = self.apply_booster_command(command)
        elif command == COMMAND_SHIFT:
            #print("About to teleport!!!")
            #time.sleep(5)
            if not command_extra in self.get_set_up_teleporter_locations():
                raise Exception("Illegal transport target!")
            command = '{}({},{})'.format(COMMAND_SHIFT, command_extra.x, command_extra.y)
            self.bot_location = command_extra
            self.update_state()
        else:
            raise NotImplementedError
        if self.save_commands:
            self.append_command_to_sequence(command)    
        self.steps = self.steps + 1
        if self.steps_wheels_remaining > 0:
            self.steps_wheels_remaining = self.steps_wheels_remaining - 1
        if self.steps_drill_remaining > 0:
            self.steps_drill_remaining = self.steps_drill_remaining - 1


    def apply_command_turn(self, direction):
        #print("Previous orientation: {} direction: {}".format(self.orientation, direction))
        self.orientation = turn_direction(self.orientation, direction)
        #print("Orientation now: {}".format(self.orientation))
        self.update_state()


    def can_move_in_direction(self, direction):
        target = self.bot_location.translate(direction)
        if self.point_in_range(target):
            if (self.get_tile(target) == TILE_OBSTACLE) and \
                    (not self.steps_drill_remaining > 0):
                #print("Running into obstacle!")
                return False
            return True
        #print("Outside range!")
        #self.print_situation()
        #print(direction)
        return False

    def point_in_range(self, point):
        return (point.x <= self.max_x) and (point.y <= self.max_y) and \
                (point.x >= 0) and (point.y >= 0)


    def basic_move(self,direction):
        if not self.can_move_in_direction(direction):
            #print("Move is {}".format(direction))
            #input("Pess key...")
            #self.print_situation()
            raise IllegalCommandError("Illegal move {}!".format(direction))
        self.bot_location = self.bot_location.translate(direction)
        self.update_state()

    def apply_command_move(self, direction):
        #print("Moving once")
        self.basic_move(direction)
        if self.steps_wheels_remaining > 0:
            #print("Moving twice")
            if self.can_move_in_direction(direction):
                self.basic_move(direction)

    def apply_booster_command(self, booster_type, extra=None):
        if not self.get_boosters_held(booster_type) > 0:
            raise Exception("Do not have enough boosters of type {}".format(booster_type))
        self.boosters_held_add(booster_type, -1)
        command = booster_type
        if booster_type == BOOSTER_FAST_WHEELS:
            #print("Attached fast wheels!")
            #time.sleep(1)
            if self.steps_wheels_remaining == 0:
                # Needed because we will immediately subtract one in apply_command
                self.steps_wheels_remaining = 1
            self.steps_wheels_remaining += 50
        elif booster_type == BOOSTER_DRILL:
            if self.steps_drill_remaining == 0:
                self.steps_drill_remaining = 1
            self.steps_drill_remaining += 30
        elif booster_type == BOOSTER_MANIPULATOR:
            vectors = self.manipulator_vectors(self.orientation)
            if vectors == manipulator_vectors_for_number_direction(len(vectors), self.orientation):
                #print("Attaching a new manipulator!")
                new_vectors = manipulator_vectors_for_number(len(vectors) + 1)
                new_manipulator = new_vectors[self.orientation][-1]
                command = 'B({},{})'.format(new_manipulator.x, new_manipulator.y)
                self.set_manipulator_vectors(new_vectors)
            else:
                raise Exception("Unknown manipulator configuration!")
            #input("Press key...")
            #time.sleep(1)
        elif booster_type == BOOSTER_TELEPORTER:
            if (self.bot_location in self.get_boosters()) and (self.get_boosters()[self.bot_location] == BOOSTER_MYSTERIOUS):
                raise IllegalCommandError("Illegaly tried to install a teleporter on top of a clone device")
            if self.bot_location in self.get_set_up_teleporter_locations():
                raise IllegalCommandError("Tried to install a teleporter on top of another one!")
            self.append_set_up_teleporter_locations(self.bot_location)

        else:
            raise NotImplementedError("Effect of booster command {} not yet implemented".\
                    format(booster_type))
        return command
        # TODO: implement booster effects


    def manipulator_locations(self):
        locations = []
        #print("Orientation is {}".format(self.orientation))
        #print("Manipulator vectors are {}".format(self.manipulator_vectors(self.orientation)))
        #print("Location is {}".format(self.bot_location))
        #print("First absolute manipulator is at {}".format(self.bot_location + self.manipulator_vectors(self.orientation)[0]))
        for vector in self.manipulator_vectors(self.orientation):
            locations.append(self.bot_location + vector)
        #print("Returning manipulator locations {}".format(locations))
        return locations

    def update_wrapped(self):
        wrap = self.manipulator_locations()
        wrap.append(self.bot_location)
        #print("wrapping locations {}".format(wrap))
        for point in wrap:
            if self.point_in_range(point):
                #print("Should be in range: {}".format(point))
                #print("max_x: {} max_y: {}".format(self.max_x, self.max_y))
                if self.get_tile(point) == TILE_EMPTY and \
                        self.is_visible(point):
                    self.set_tile(point, TILE_DONE)
                    self.empty = self.empty - 1

    def update_state(self):
        self.update_wrapped()
        boosters = self.get_boosters()
        if self.bot_location in boosters:
            booster_type = boosters[self.bot_location]
            if booster_type != BOOSTER_MYSTERIOUS:
                self.del_booster(self.bot_location)
                self.boosters_held_add(booster_type, 1)
        if self.get_tile(self.bot_location) == TILE_OBSTACLE:
            self.set_tile(self.bot_location, TILE_DONE)


    def is_visible(self, point):
        if self.bot_location.x == point.x:
            for y in full_range(self.bot_location.y,point.y):
                if self.get_tile(Point(point.x,y)) == TILE_OBSTACLE:
                    #print("Not visible, on same row")
                    return False
            #print("Visible, on same row")
            return True
        elif self.bot_location.y == point.y:
            for x in full_range(self.bot_location.x,point.x):
                if self.get_tile(Point(x,point.y)) == TILE_OBSTACLE:
                    #print("Not visible, on same column")
                    return False
            #print("Visible, on same column")
            return True
        elif self.bot_location.x < point.x:
            point_left = self.bot_location
            point_right = point
        else:
            point_left = point
            point_right = self.bot_location
        # double all distances to not get halves
        dx = 2*(point_right.x - point_left.x)
        dy = 2*(point_right.y - point_left.y)
        if self.get_tile(point_left) == TILE_OBSTACLE:
            #print("Not visible, located on obstacle.")
            return False
        for x in full_range(point_left.x, point_right.x):
            for y in full_range(point_left.y, point_right.y):
                #print("{} {}".format(x,y))
                if self.get_tile(Point(x,y)) == TILE_OBSTACLE:
                    dxtest_left   = (2*(x - point_left.x)-1)
                    dxtest_right  = (2*(x - point_left.x)+1)
                    dytest_top    = (2*(y - point_left.y)+1)
                    dytest_bottom = (2*(y - point_left.y)-1)
                    if point_left.y < point_right.y:
                        if (dytest_top*dx > dxtest_left*dy) and \
                                (dytest_bottom*dx < dxtest_right*dy):
                            #print("Not visible, case 1")
                            return False
                    if point_left.y > point_right.y:
                        if (dytest_top*dx > dxtest_right*dy) and \
                                (dytest_bottom*dx < dxtest_left*dy):
                            #print("Not visible, case 2")
                            return False
        #print("Visible, in general position")
        return True

    def print_changes(self):
        print(self.__dict__)

class Situation:
    def __init__(self, task_filename=None, problem_name=''):
        self.steps = 0
        self.boosters_held = {}
        for booster_type in BOOSTER_TYPES:
            self.boosters_held[booster_type] = 0
        self.command_sequence = []
        self.problem_name = problem_name
        self.paths = None
        self.paths2 = None
        self.orientation = DIRECTION_RIGHT
        self.steps_wheels_remaining = 0
        self.steps_drill_remaining = 0
        self.set_up_teleporter_locations = []
        if task_filename:
            self.load_from_task_description(task_filename)


    def point_in_range(self, point):
        return (point.x <= self.max_x) and (point.y <= self.max_y) and \
                (point.x >= 0) and (point.y >= 0)

    def load_from_task_description(self, task_filename):
        #print('Loading situation from task description '
        #    '{}'.format(task_filename))
        if __debug__:
            a = time.perf_counter()
        with open(task_filename) as f:
            data = f.read()
        data = data.split(sep='#')
        if __debug__:
            b = time.perf_counter()
        self.load_bounding_polygon_desc(data[0])
        if __debug__:
            c = time.perf_counter()
        self.setup_map_tiles()
        if __debug__:
            d = time.perf_counter()
        self.load_blocked_by_bounding_polygon()
        if __debug__:
            e = time.perf_counter()
        self.load_bot(data[1])
        if __debug__:
            f = time.perf_counter()
        self.load_obstacles(data[2])
        if __debug__:
            g = time.perf_counter()
        self.load_boosters(data[3])
        self.set_up_teleporter_rectangles()
        if __debug__:
            h = time.perf_counter()
        self._number_of_empty_tiles()
        #TODO: Use a delta here etc. instead!
        delta = SituationDelta(parent_situation=self)
        delta.update_state()
        self.apply_delta(delta)
        if __debug__:
            i = time.perf_counter()
        if __debug__:
            t=i-a
            print('Performance(loading task): \nread: {}\npolygon: {}\n\
tiles: {}\nblocked: {}\nbot: {}\nobstacles: {}\n\
boosters: {}\nstate: {}\ntotal: {}'.format((b-a)/t,\
                    (c-b)/t,(d-c)/t,(e-d)/t,(f-e)/t,(g-f)/t,(h-g)/t,(i-h)/t,t))

        #print('Data is {}'.format(data))

    def load_bounding_polygon_desc(self, polygon_desc):
        self.bounding_polygon = PolygonChain(polygon_desc, closed=True, from_string=True)

    def setup_map_tiles(self):
        self.max_x = max([point.x for point in self.bounding_polygon.chain]) - 1
        self.max_y = max([point.y for point in self.bounding_polygon.chain]) - 1
        self.map_tiles = []
        for x in range(0,self.max_x + 1):
            self.map_tiles.append([])
            for y in range(0,self.max_y + 1):
                self.map_tiles[x].append(TILE_OBSTACLE)

    def print_situation(self, path_mode=False, path_length=10, space='', wait_seconds=0):
        #FIXME: currently displays the manipulators wrong because it isn't
        # using the new vector format
        global time_last_print
        if wait_seconds > 0:
            time_now = time.perf_counter()
            if (time_now - time_last_print) < wait_seconds:
                return
            else:
                time_last_print = time_now
        print('\033c')
        if path_mode:
            self.calculate_paths(max_length=path_length)
        for y in range(self.max_y,-1,-1):
            for x in range(0,self.max_x + 1):
                point = Point(x,y)
                if self.map_tiles[x][y] == TILE_DONE:
                    print(BACKGROUND_COLOR_GREEN, end='')
                if self.bot_location == Point(x,y):
                    print(BACKGROUND_COLOR_RED, end='')
                if Point(x,y) in self.manipulator_locations():
                    print(BACKGROUND_COLOR_YELLOW, end='')
                if Point(x,y) in self.booster_locations:
                    print(BACKGROUND_COLOR_CYAN, end='')
                    if self.booster_type[point] == BOOSTER_MANIPULATOR:
                        print(FOREGROUND_COLOR_YELLOW, end='')
                    elif self.booster_type[point] == BOOSTER_FAST_WHEELS:
                        print(FOREGROUND_COLOR_MAGENTA, end='')
                    elif self.booster_type[point] == BOOSTER_DRILL:
                        print(FOREGROUND_COLOR_GREEN, end='')
                if self.map_tiles[x][y] == TILE_OBSTACLE:
                    print(BACKGROUND_COLOR_BLACK + TILE_OBSTACLE + space, end='')
                else:
                    if path_mode and point in self.points_distance:
                        print(str(self.points_distance[point]) + space, end='')
                    else:
                        print(self.map_tiles[x][y] + space, end='')
                print(BACKGROUND_COLOR_DEFAULT + FOREGROUND_COLOR_DEFAULT, end='')
            print("")
        print("Problem: {} Steps: {} Empty: {} Boosters: {}".format(self.problem_name, self.steps, self.empty, self.boosters_held))
        print("Wheels: {} Drill: {} Manipulator: {} Teleporter: {}".format(
            self.boosters_held[BOOSTER_FAST_WHEELS],
            self.boosters_held[BOOSTER_DRILL],
            self.boosters_held[BOOSTER_MANIPULATOR],
            self.boosters_held[BOOSTER_TELEPORTER]))
        print("Effects: wheels: {} drill: {}".format(
            self.steps_wheels_remaining, self.steps_drill_remaining))
        print("Teleporter locations: {}".format(self.set_up_teleporter_locations))

    def manipulator_locations(self):
        locations = []
        #print("Orientation is {}".format(self.orientation))
        #print("Manipulator vectors are {}".format(self.manipulator_vectors(self.orientation)))
        #print("Location is {}".format(self.bot_location))
        #print("First absolute manipulator is at {}".format(self.bot_location + self.manipulator_vectors(self.orientation)[0]))
        for vector in self.manipulator_vectors[self.orientation]:
            locations.append(self.bot_location + vector)
        #print("Returning manipulator locations {}".format(locations))
        return locations

    # Move along the map and note down which tiles are inside/outside the
    # bounding polygon.
    def load_blocked_by_bounding_polygon(self):
        if __debug__:
            a = time.perf_counter()
        inside = self.bounding_polygon.tiles_inside()
        if __debug__:
            b = time.perf_counter()
        for point in inside:
            self.map_tiles[point.x][point.y] = TILE_EMPTY
        if __debug__:
            c = time.perf_counter()
        if __debug__:
            t = c-a
            print("Performance(load blocked): tiles_inside {}, update: {}".format((b-a)/t,(c-b)/t))


    def load_bot(self, location):
        self.bot_location = Point(from_string=location)
        #self.bot_manipulators = [self.bot_location.translate(DIRECTION_RIGHT),
        #        self.bot_location.translate(DIRECTION_RIGHT).translate(DIRECTION_UP),
        #        self.bot_location.translate(DIRECTION_RIGHT).translate(DIRECTION_DOWN)]
        self.manipulator_vectors = {
                DIRECTION_RIGHT: manipulator_vectors_for_number_direction(3,DIRECTION_RIGHT),
                DIRECTION_LEFT: manipulator_vectors_for_number_direction(3,DIRECTION_LEFT),
                DIRECTION_UP: manipulator_vectors_for_number_direction(3,DIRECTION_UP),
                DIRECTION_DOWN: manipulator_vectors_for_number_direction(3,DIRECTION_DOWN)
                }
        self.bot_bounding_delta_x = 1
        self.bot_bounding_delta_y = 1
        self.bot_bounding_delta_max = 1

    def load_obstacles(self, obstacles):
        obstacles = obstacles.split(sep=';')
        if obstacles == ['']:
            obstacles = []
        obstacles = [PolygonChain(obstacle,closed=True,from_string=True) for obstacle in obstacles]
        for obstacle in obstacles:
            for point in obstacle.tiles_inside():
                self.map_tiles[point.x][point.y] = TILE_OBSTACLE

    def load_boosters(self, boosters):
        self.booster_locations = []
        self.booster_type = {}
        boosters = boosters.split(sep=';')
        if boosters == ['']:
            boosters = []
        for booster in boosters:
            booster_type = booster[0]
            booster_location = Point(from_string=booster[1:])
            self.booster_locations.append(booster_location)
            self.booster_type[booster_location]=booster_type

    def set_up_teleporter_rectangles(self):
        num_teleporters = 0
        for location in self.booster_type:
            if self.booster_type[location] == BOOSTER_TELEPORTER:
                num_teleporters += 1
        num_divisions = math.ceil(math.sqrt(num_teleporters))
        #print("Number of teleporters on the map is {}, divisions {}".format(
        #    num_teleporters, num_divisions))
        #input("Press key...")
        self.teleporter_divisions = num_divisions

    def calculate_paths(self, max_length=None):
        #paths = {self.bot_location: (0,[])}
        if self.paths is None:
            self.paths = [{self.bot_location: []}]
            start_length = 1
            self.points_distance = {self.bot_location: 0}
        else:
            start_length = len(self.paths)
                #if start_length > 10:
            #print("start_length: {}".format(start_length))
            #input("Press key...")
        for length in range(start_length,max_length + 1):
            added_something = False
            self.paths.append({})
            for start in self.paths[length - 1]:
                for direction in DIRECTIONS:
                    target = start.translate(direction)
                    #print("target.y: {} self.max_y: {}".format(target.y, self.max_y))
                    if (target.x <= self.max_x) and (target.y <= self.max_y) and \
                            (target.x >= 0) and (target.y >= 0):
                        if (not target in self.points_distance) and \
                                (not self.map_tiles[target.x][target.y] == TILE_OBSTACLE):
                            self.paths[length][target] = self.paths[length - 1][start] + [direction]
                            self.points_distance[target]=length
                            added_something = True
            if not added_something:
                break

    # self.paths[number of steps necessary (as in time)][location] =
    # (sequence of moves to get there, ending direction (DIRECTION_IRRELEVANT if fast
    # wheels already used in this step))
    # FIXME: make this find faster routes if coming from a certain direction would
    # be helpful for fast wheels but we already used up the target by reaching it from
    # elsewhere
    def calculate_paths2(self, max_length=None):
        #paths = {self.bot_location: (0,[])}
        if self.paths2 is None:
            self.paths2 = [{self.bot_location: ([],DIRECTION_IRRELEVANT)}]
            start_length = 1
            self.points_distance2 = {self.bot_location: 0}
        else:
            start_length = len(self.paths2)
                #if start_length > 10:
            #print("start_length: {}".format(start_length))
            #input("Press key...")
        for length in range(start_length,max_length + 1):
            added_something = False
            self.paths2.append({})
            for start in self.paths2[length - 1]:
                if self.paths2[length-1][start][1] == DIRECTION_IRRELEVANT:
                    for direction in DIRECTIONS:
                        target = start.translate(direction)
                        #print("target.y: {} self.max_y: {}".format(target.y, self.max_y))
                        if (target.x <= self.max_x) and (target.y <= self.max_y) and \
                                (target.x >= 0) and (target.y >= 0) and \
                                (not target in self.points_distance2) and \
                                (not self.map_tiles[target.x][target.y] == TILE_OBSTACLE):
                            added_second = False
                            target_1 = target
                            path_comp_1 = self.paths2[length - 1][start][0] + [direction]
                            #self.paths2[length][target] = (path_comp, direction)
                            self.points_distance2[target]=length
                            added_something = True
                            if self.steps_wheels_remaining >= length:
                                target = target.translate(direction)
                                if (target.x <= self.max_x) and (target.y <= self.max_y) and \
                                        (target.x >= 0) and (target.y >= 0) and \
                                        (not target in self.points_distance2) and \
                                        (not self.map_tiles[target.x][target.y] == TILE_OBSTACLE):
                                    self.paths2[length][target] = \
                                        (self.paths2[length - 1][start][0] + [direction],
                                                DIRECTION_IRRELEVANT)
                                    self.points_distance2[target]=length
                                    added_second = True
                            if added_second:
                                self.paths2[length][target_1] = (path_comp_1, direction)
                            else:
                                self.paths2[length][target_1] = (path_comp_1, DIRECTION_IRRELEVANT)
            if not added_something:
                break


    def _number_of_empty_tiles(self):
        self.empty = 0
        for x in range(0, self.max_x + 1):
            for y in range(0, self.max_y + 1):
                if self.map_tiles[x][y] == TILE_EMPTY:
                    self.empty = self.empty + 1

    def _empty_tiles(self):
        empty_list = []
        for x in range(0, self.max_x + 1):
            for y in range(0, self.max_y + 1):
                if self.map_tiles[x][y] == TILE_EMPTY:
                    empty_list.append(Point(x,y))
        return empty_list


    def commands_to_string(self):
        return ''.join(self.command_sequence)

    # If delta is without commands, they need to be provided
    def apply_delta(self, delta, commands=None):
        #print("Applying a delta to the situation")
        #print(delta.__dict__)
        delta_dict = delta.__dict__
        for key in delta_dict:
            if key == '_map_tiles_delta':
                #FIXME: make map_tiles into dict
                #self.map_tiles.update(delta_dict[key])
                tiles = delta_dict[key]
                for point in tiles:
                    self.map_tiles[point.x][point.y] = tiles[point]
            elif key == 'save_commands':
                if delta_dict[key]:
                    self.command_sequence.extend(delta_dict['_command_sequence_addition'])
                else:
                    self.command_sequence.extend(commands)
            elif key == '_command_sequence_addition':
                pass
            elif key == '_set_up_teleporter_locations_addition':
                self.set_up_teleporter_locations.extend(delta_dict[key])
            elif key == '_bot_location':
                self.bot_location = delta_dict[key]
            elif key == '_empty':
                self.empty = delta_dict[key]
            elif key == '_booster_locations_deleted':
                boosters_deleted = delta_dict[key]
                for location in boosters_deleted:
                    self.booster_locations.remove(location)
                    del self.booster_type[location]
            elif key == '_steps':
                self.steps = delta_dict[key]
            elif key == 'parent_situation':
                if id(delta_dict[key]) != id(self):
                    raise Exception("Trying to apply a delta to the wrong situation!")
            elif key == '_boosters_held_change':
                for booster_type in delta_dict[key]:
                    self.boosters_held[booster_type] += delta_dict[key][booster_type]
            elif key == '_orientation':
                self.orientation = delta_dict[key]
            elif key == '_steps_wheels_remaining':
                self.steps_wheels_remaining = delta_dict[key]
            elif key == '_steps_drill_remaining':
                self.steps_drill_remaining = delta_dict[key]
            elif key == '_manipulator_vectors':
                self.manipulator_vectors = delta_dict[key]
            else:
                raise NotImplementedError("Unknown key in delta: {}".format(key))

#TODO: implement this to filter deltas with the same location
# This only returns true if delta1 is unequivocally better or equal,
# in a strict way.
def is_delta_better(delta1, delta2):
    if delta2.bot_location != delta1.bot_location:
        return False
    if delta2.empty < delta1.empty:
        return False
    if delta2.steps < delta1.steps:
        return False
    if delta2.orientation != delta1.orientation:
        return False
    for booster_type in BOOSTER_TYPES:
        if delta2.get_boosters_held(booster_type) > delta1.get_boosters_held(booster_type):
            return False
    # Need not equal here, otherwise it might happen that with wheels one does
    # not get out of a room with only a width-one exit.
    if delta2.steps_wheels_remaining != delta1.steps_wheels_remaining:
        return False
    if delta2.steps_drill_remaining > delta1.steps_drill_remaining:
        return False
    if len(delta2.manipulator_vectors(delta2.orientation)) > len(delta1.manipulator_vectors(delta1.orientation)):
        return False
    if len(delta2.get_set_up_teleporter_locations()) > len(delta1.get_set_up_teleporter_locations()):
        return False
    #TODO: implement other criteria, such as boosters
    return True

def calculate_empty_side_score_situation(situation):
    empties = situation._empty_tiles()
    score = 0
    for empty in empties:
        for direction in DIRECTIONS:
            target = empty.translate(direction)
            if (not situation.point_in_range(target)) or \
                    (not situation.map_tiles[target.x][target.y] == TILE_EMPTY):
                score -= 1
    return score

def calculate_empty_side_score_delta(situation, delta):
    score = 0
    for point in delta._map_tiles_delta:
        # change relevant for us is only if it was empty before
        # and isn't now.
        if situation.map_tiles[point.x][point.y] == TILE_EMPTY:
            for direction in DIRECTIONS:
                target = point.translate(direction)
                # if the target is not empty in situation, then in situation
                # we got a minus point for this tile.
                # so give it back.
                if (not situation.point_in_range(target)) or \
                        (not situation.map_tiles[target.x][target.y] == TILE_EMPTY):
                    score += 1
                # If the other tile *is* empty *now*, then it gets a
                # minus point now.
                if (situation.point_in_range(target)) and \
                        (delta.get_tile(target) == TILE_EMPTY):
                    score -= 1
    #print("For delta with commands {}, empty side score delta is {}".\
    #        format(delta._command_sequence_addition, score))
    return score

# scoring system: the higher the value, the better
def evaluate_delta(situation, delta):
    score = 0

    number_of_manipulators = len(delta.manipulator_vectors(delta.orientation))
    steps = delta.steps - situation.steps
    empty = situation.empty - delta.empty

    # deduct 20 points for each step
    #score -= 10 * (delta.steps - situation.steps)

    # add 10 points for each empty that has been wrapped
    #score += 10 * (situation.empty - delta.empty)

    score += 10 * (empty / steps)

    # isolated empty tiles give 8 minus points
    score += 20 * calculate_empty_side_score_delta(situation, delta)

    score += delta.steps_wheels_remaining * (number_of_manipulators / 5)
    score += delta.steps_drill_remaining * 0.9

    score += (delta.get_boosters_held(BOOSTER_FAST_WHEELS) * 50 * (number_of_manipulators / 5))
    score += delta.get_boosters_held(BOOSTER_DRILL) * 30
    score += delta.get_boosters_held(BOOSTER_MANIPULATOR) * 200
    score += delta.get_boosters_held(BOOSTER_TELEPORTER) * 200

    if delta.get_command_sequence()[-1][0] == 'B':
        # give a massive bonus to attaching a manipulator
        score += 100000

    return score

def best_delta(situation, deltas):
    deltas_solve_completely = [delta for delta in deltas if delta.empty == 0]
    if len(deltas_solve_completely) > 0:
        steps_list = [delta.steps for delta in deltas_solve_completely]
        min_steps = min(steps_list)
        best_solution = deltas_solve_completely[steps_list.index(min_steps)]
        return best_solution

    #print("Evaluating out of {} deltas".format(len(deltas)))
    #min_steps = min([delta.steps for delta in deltas])
    #up_to_steps = int((min_steps * 2) + 5)
    #deltas = [delta for delta in deltas if delta.steps <= up_to_steps]
    #print("Minumum steps are {}, looking at the {} deltas with up to {} steps".format(
    #        min_steps, len(deltas), up_to_steps))
    scores = [evaluate_delta(situation, delta) for delta in deltas]
    max_score = max(scores)
    best_delta = deltas[scores.index(max_score)]
    #print("Scores are {}".format(scores))
    #input("Press key...")
    return best_delta

def delta_solver1(situation, time_budget=None):
    if not time_budget is None:
        time_start = time.perf_counter()
    initial_delta = SituationDelta(situation)
    deltas = {
        situation.bot_location: [(initial_delta)]
        }
    deltas_to_start_from_next = [deltas[situation.bot_location][0]]
    found_reducing_empty = False
    stop_search = False
    hurry_up = False
    if time_budget is None:
        hurry_up = True
    deltas_reducing_empty = []
    # Always immediately attach new manipulators
    if initial_delta.get_boosters_held(BOOSTER_MANIPULATOR) > 0:
        initial_delta.apply_command(BOOSTER_MANIPULATOR)
        situation.apply_delta(initial_delta)
        return situation.empty - initial_delta.empty
    # Always check if we should drop the teleporter
    if initial_delta.get_boosters_held(BOOSTER_TELEPORTER) > 0:
        #print("Checking if we should drop the teleporter")
        x_num = math.floor(situation.teleporter_divisions * \
                situation.bot_location.x / situation.max_x)
        y_num = math.floor(situation.teleporter_divisions * \
                situation.bot_location.y / situation.max_y)
        #print("x_num: {}, y_num: {}".format(x_num, y_num))
        drop_it = True
        for location in situation.set_up_teleporter_locations:
            if (x_num ==  math.floor(situation.teleporter_divisions * \
                    location.x / situation.max_x)) or \
                    (y_num ==  math.floor(situation.teleporter_divisions * \
                    location.y / situation.max_y)):
                drop_it = False
        #print("Should we drop the teleporter? {}".format(drop_it))
        if drop_it:
            initial_delta.apply_command(BOOSTER_TELEPORTER)
            situation.apply_delta(initial_delta)
            #time.sleep(5)
            return situation.empty - initial_delta.empty
    emergency_reset = False
    while True:
        if not time_budget is None:
            time_needed = time.perf_counter() - time_start
            if time_needed > time_budget:
                hurry_up = True
        # Calculate where I can go with one more command.
        # Loop over previous end points:
        #print("Have already {} locations, finding more starting from {} deltas."\
        #        .format(len(list(deltas.keys())), len(deltas_to_start_from_next)))
        deltas_to_start_from = deltas_to_start_from_next
        deltas_to_start_from_next = []
        for delta in deltas_to_start_from:
            # TODO: also do other commands
            commands_avail = copy.deepcopy(COMMANDS)
            for teleporter_location in delta.get_set_up_teleporter_locations():
                commands_avail.append((COMMAND_SHIFT, teleporter_location))
            for command in commands_avail:
                extra = None
                if isinstance(command, tuple):
                    extra = command[1]
                    command = command[0]
                #print("Trying command {}".format(command))
                if (command in BOOSTER_TYPES) and \
                        (not delta.get_boosters_held(command) > 0):
                    continue
                try:
                    new_delta = SituationDelta(command=command, from_delta=delta, command_extra=extra)
                except IllegalCommandError:
                    #print("Was an illegal command!")
                    continue
                #print("Was not an illegal command!")
                new_location = new_delta.bot_location
                should_add = True
                if hurry_up and len(deltas.setdefault(new_location,[])) > 0 and \
                        (not emergency_reset):
                    should_add = False
                else:
                    for competing_delta in deltas.setdefault(new_location,[]):
                        if is_delta_better(competing_delta, new_delta):
                            should_add = False
                if should_add:
                    deltas[new_location].append(new_delta)
                    deltas_to_start_from_next.append(new_delta)
                    if new_delta.empty < situation.empty:
                        found_reducing_empty = True
                        deltas_reducing_empty.append(new_delta)
            if hurry_up and found_reducing_empty:
                #print("Stopping search because time is up...")
                stop_search = True
                break
        #print("Found the following deltas:")
        if len(deltas_to_start_from_next) == 0:
            #print("Stopping search as there is nothing more to do...")
            if not found_reducing_empty:
                #We might not have found anything due to hurrying and hence
                # not looking at all paths. so restart search now including them.
                if not emergency_reset:
                    # Use this only once
                    emergency_reset = True
                    deltas_to_start_from_next = []
                    for loc in deltas:
                        deltas_to_start_from_next.extend(deltas[loc])
                else:
                    situation.print_situation()
                    print(deltas)
                    raise Exception("Could not find further options yet did not find even one reducing empty")
            else:
                stop_search = True

            #situation.print_situation()
            #print(deltas)
            #for deltalist in deltas.values():
            #    for delta in deltalist:
            #        if delta._map_tiles_delta != {}:
            #            delta.print_changes()
            #print("Empty tiles: {}".format(situation._empty_tiles()))
            ##print(deltas[Point(42,99)])
            #raise Exception("Can not start from anywhere???")
        #print(deltas)
        #print("Actual list of deltas")
        #input("Press key...")
        if stop_search:
            break
    
    delta = best_delta(situation, deltas_reducing_empty)
    situation.apply_delta(delta)
    #if not time_budget is None:
    #    time_needed = time.perf_counter() - time_start
        #print("Timing: Needed {}% of the alotted time {}".\
        #        format(100*(time_needed/time_budget),time_budget))
    return situation.empty - delta.empty

# just goes to the next empty tile the fasted way possible
def path_solver3(situation):
    situation.print_situation(path_mode=True, path_length=4)
    print_wait = (situation.max_x * situation.max_y) / 10000
    if __debug__:
        time_paths = 0
        time_moving = 0
        time_printing = 0
        time_total = 0
    while situation.empty > 0:
        if __debug__:
            time_start_loop = time.perf_counter()
        situation.calculate_paths(max_length=0)
        previous_length=0
        for length in range(10,1000,10):
            #print("Calculation paths of length {}".format(length))
            situation.calculate_paths(max_length=length)
            previous_length = length
            #print("Searching the closest empty tile")
            found_something = False
            for dist in range(0, len(situation.paths)):
                #print("Looking at distance {}".format(dist))
                for point in situation.paths[dist]:
                    #print("There is a path to {}".format(point))
                    if situation.map_tiles[point.x][point.y] == TILE_EMPTY:
                        path = situation.paths[dist][point]
                        #print("Found a target to move to.")
                        found_something = True
                        break
                if found_something:
                    break
            if found_something:
                break
            #input("Press key...")
        if __debug__:
            time_after_paths = time.perf_counter()
        #print("Path is {}".format(path))
        if len(path) > situation.bot_bounding_delta_max:
            # Lets do more steps, nothing can change in between anyway
            # as long as we don't use any boosters etc.
            path = path[0:len(path)-situation.bot_bounding_delta_max]
        else:
            path = path[0:1]
        for direction in path:
            #print("Will make move {}".format(direction))
            #input("Press key...")
            situation.command_move(direction)
        if __debug__:
            time_after_moving = time.perf_counter()
        situation.print_situation(path_mode=True, path_length=4, wait_seconds=print_wait)
        if __debug__:
            time_after_printing = time.perf_counter()
        if __debug__:
            time_paths    = time_paths + time_after_paths - time_start_loop
            time_moving   = time_moving + time_after_moving - time_after_paths
            time_printing = time_printing + time_after_printing - time_after_moving
            time_total = time_paths + time_moving + time_printing
            print("Performance(path_solver loop): paths: {} moving: {} printing: {}".format(
                time_paths/time_total, time_moving/time_total, time_printing/time_total))
        #input("Press key...")
    situation.print_situation(path_mode=True, path_length=4)
    #print("Solved problem!")
    #print(situation.commands_to_string())
    return situation

# applies boosters if it happens to have them, but does not seek them out
# or use them strategically
def path_solver5(situation):
    #situation.print_situation(path_mode=True, path_length=4)
    print_wait = (situation.max_x * situation.max_y) / 10000
    if __debug__:
        time_paths = 0
        time_moving = 0
        time_printing = 0
        time_total = 0
    while situation.empty > 0:
        if __debug__:
            time_start_loop = time.perf_counter()
        if situation.boosters_held[BOOSTER_FAST_WHEELS] > 0:
            situation.command_booster(BOOSTER_FAST_WHEELS)
        situation.calculate_paths2(max_length=0)
        previous_length=0
        #print("A")
        for length in range(10,1000,10):
            #print("up to length: {}".format(length))
            print("Calculation paths of length {}".format(length))
            situation.calculate_paths2(max_length=length)
            #print(situation.paths2)
            previous_length = length
            #print("Searching the closest empty tile")
            found_something = False
            for dist in range(0, len(situation.paths2)):
                #print("Looking at distance {}".format(dist))
                for point in situation.paths2[dist]:
                    #print("There is a path to {}".format(point))
                    if situation.map_tiles[point.x][point.y] == TILE_EMPTY:
                        path = situation.paths2[dist][point][0]
                        #print("Found a target to move to:")
                        #print(point)
                        #print(situation.paths2[dist][point])
                        found_something = True
                        break
                if found_something:
                    break
            if found_something:
                break
            #input("Press key...")
        if not found_something:
            situation.print_situation()
            print(situation.paths2)
            raise Exception("Could not solve it...")
        #print("B")
        if __debug__:
            time_after_paths = time.perf_counter()
        print("Path is {}".format(path))
        if len(path) > situation.bot_bounding_delta_max:
            # Lets do more steps, nothing can change in between anyway
            # as long as we don't use any boosters etc.
            path = path[0:len(path)-situation.bot_bounding_delta_max]
        else:
            path = path[0:1]
        print("Starting move sequence")
        for direction in path:
            print("Will make move {}".format(direction))
            print("Whole path is {}".format(path))
            #input("Press key...")
            situation.command_move(direction)
        if __debug__:
            time_after_moving = time.perf_counter()
        #situation.print_situation(path_mode=True, path_length=4, wait_seconds=print_wait)
        #input("Press key...")
        if __debug__:
            time_after_printing = time.perf_counter()
        if __debug__:
            time_paths    = time_paths + time_after_paths - time_start_loop
            time_moving   = time_moving + time_after_moving - time_after_paths
            time_printing = time_printing + time_after_printing - time_after_moving
            time_total = time_paths + time_moving + time_printing
            print("Performance(path_solver loop): paths: {} moving: {} printing: {}".format(
                time_paths/time_total, time_moving/time_total, time_printing/time_total))
        #input("Press key...")
    #situation.print_situation(path_mode=True, path_length=4)
    #print("Solved problem!")
    #print(situation.commands_to_string())
    return situation



class Solver():
    def __init__(self, name=None, version=None, solver=None):
        self.name = name
        self.version = version
        self.solver = solver
        self.situation = None

    def run(self, problem_number, silent=False, time_budget=None, time_budget_per_tile=None):
        problem_number_string = str(problem_number).zfill(3)
        #print("Running solver on problem number {}".format(problem_number_string))
        path_problem  = base_path_problems  + 'prob-' + problem_number_string + '.desc'
        time_before = int(time.mktime(time.gmtime()))
        self.situation = Situation(task_filename=path_problem, problem_name=problem_number_string)
        time_after_load = int(time.mktime(time.gmtime()))
        if not silent:
            self.situation.print_situation(path_mode=True, path_length=4)
        print_wait = (self.situation.max_x * self.situation.max_y) / 3000
        #print_wait = 0.2

        if not time_budget_per_tile is None:
            time_budget = self.situation.max_x * self.situation.max_y * time_budget_per_tile
        if not time_budget is None:
            # Get at least this amount of time per step
            time_base_per_step = (time_budget / self.situation.empty) * 10
            time_start = time.perf_counter()
            time_total = time_budget

        while self.situation.empty > 0:
            if not time_budget is None:
                time_elapsed = time.perf_counter() - time_start
                time_remaining = time_total - time_elapsed
                time_budget = time_base_per_step
                if (time_remaining / self.situation.empty) > time_base_per_step:
                    time_budget = time_remaining / self.situation.empty
                time_budget = time_budget * 0.75 # Estimate of overhead
                #print("Giving solver {}s for running one step, \
#elapsed time is {} and remaining is {}".format(\
#                        time_budget, time_elapsed, time_remaining))
            ret = self.run_solver_step(time_budget=time_budget)
            if ret < 0:
                raise Exception("Solver has an error")
            #input("Press key...")
            if not silent:
                self.situation.print_situation(path_mode=True, path_length=4, wait_seconds=print_wait)

        time_after = int(time.mktime(time.gmtime()))
        if not silent:
            self.situation.print_situation(path_mode=True, path_length=4)
        if self.situation.empty > 0:
            print("Solver found no solution for problem {}".format(problem_number_string))
            return None
        else:
            steps = self.situation.steps
            solution_string = self.situation.commands_to_string()
        runtime_load = time_after_load - time_before
        runtime = time_after - time_before
        if __debug__:
            print("Loadtime: {} Total runtime: {}".format(runtime_load, runtime))
        solution = RichSolution(solution=solution_string, steps=steps,
                timestamp=int(time.mktime(time.gmtime())),
                runtime=runtime, solver=self.name, solver_version=self.version,
                problem_id=problem_number)
        solution.write_to_disk()
        self.situation = None
        return solution

    def run_solver_step(self, time_budget=None):
        # The solver changes the situation
        return self.solver(self.situation, time_budget=time_budget)

class ProblemSolutionInfo():
    def __init__(self,problem_filename):
        #print("Loading information for problem {}".format(problem_filename))
        self.problem_filename = problem_filename
        self.problem_id = int(problem_filename.split(sep='.')[0].split(sep='-')[1])
        self.solutions = []
        solutions = pathlib.Path(base_path_solutions) / problem_filename.split(sep='.')[0]
        if solutions.exists():
            for solution in solutions.iterdir():
                #print("Loading solution {}".format(solution))
                sol = RichSolution()
                sol.read_from_disk(solution)
                self.solutions.append(sol)

    def __lt__(self,other):
        return self.problem_id.__lt__(other.problem_id)

    def __str__(self):
        return self.problem_id

    def __repr__(self):
        return 'ProblemSolutionInfo({})'.format(self.problem_filename)

    def print(self):
        print("Problem id: {}".format(self.problem_id))
        print("filename: {}".format(self.problem_filename))
        print("Solutions:")
        print(self.solutions)

    def solvers_tried(self):
        solvers = set()
        for solution in self.solutions:
            #print(solution)
            solvers.add((solution.solver,solution.solver_version))
        return solvers

    def best_solution(self):
        best_sol = None
        for sol in self.solutions:
            if (best_sol is None) or (best_sol.steps > sol.steps):
                best_sol = sol
        return best_sol

    def best_solution_solver(self, name, version):
        best_sol = None
        for sol in self.solutions:
            if (sol.solver == name and sol.solver_version == version):
                if (best_sol is None) or (best_sol.steps > sol.steps):
                    best_sol = sol
        return best_sol

    def last_solution_timestamp(self):
        timestamp = 0
        for sol in self.solutions:
            if sol.timestamp > timestamp:
                timestamp = sol.timestamp
        return timestamp


class Manager():
    def __init__(self):
        self.newly_solved_problems = False
        self.last_submission_best_team_steps = {}
        self.solution_archives={}
        self._lock = threading.Lock()

        self.load_problem_solution_information()
        self.load_manager_configuration()
        self.solvers=[]
        self.solvers.append(Solver(solver=path_solver3,name='path',version=3))
        self.solvers.append(Solver(solver=None,name='path',version=0))
        self.solvers.append(Solver(solver=None,name='path',version=4))
        self.solvers.append(Solver(solver=None,name='path',version=5))
        self.solvers.append(Solver(solver=delta_solver1, name='delta', version=delta_solver_version))
        self.solvers.append(Solver(solver=None, name='delta', version=1))
        self.solvers.append(Solver(solver=None, name='delta', version=2))
        self.solvers.append(Solver(solver=None, name='delta', version=3))
        self.solvers.append(Solver(solver=None, name='delta', version=4))
        self.solver_best=self.solvers[4]

    def load_manager_configuration(self):
        path = pathlib.Path(path_manager_configuration)
        with path.open() as f:
            content = f.read()
        #print("Manager: Loading {}".format(content))
        self.__dict__.update(json.loads(content))
        #self.total_steps = int(self.total_steps)
        #self.last_submission_timestamp = int(self.last_submission_timestamp 

    def save_manager_configuration(self):
        content = json.dumps(
                {
                    'team_name': self.team_name,
                    'api_id': self.api_id,
                    'last_solution_submission_timestamp': self.last_solution_submission_timestamp,
                    'total_steps': self.total_steps,
                    'total_steps_submitted': self.total_steps_submitted,
                    'newly_solved_problems': self.newly_solved_problems,
                    'last_submission_best_team_steps': self.last_submission_best_team_steps,
                    'solution_archives': self.solution_archives
            })
        path = pathlib.Path(path_manager_configuration)
        with path.open('w') as f:
            content = f.write(content)

    def load_problem_solution_information(self):
        self.problems = {}
        #print("Loading problem and solution information")
        problems = pathlib.Path(base_path_problems)
        for problem in problems.iterdir():
            prob = ProblemSolutionInfo(problem.name)
            self.problems[prob.problem_id] = prob
            #self.problems[-1].print()

    def num_problems_without_solution(self):
        return len(self.problems_without_solution())

    def problems_without_solution(self):
        return [problem_id for problem_id in self.problems if \
                len(self.problems[problem_id].solutions)==0]

    def print(self):
        print("API id: {}".format(self.api_id))
        print("Problems without solution: {}".format(self.num_problems_without_solution()))
        print("Last solution submission was {} seconds ago.".format(
            int(time.mktime(time.gmtime())) - self.last_solution_submission_timestamp))
        print("Total steps needed:    {}".format(self.total_steps))
        print("Total steps submitted: {}".format(self.total_steps_submitted))
        print("Have newly solved problems: {}".format(self.newly_solved_problems))

        #print("Solution information:")
        #print(self.problems)
        #print(self.problems[4].solvers_tried())
        #print(self.problems[4].best_solution())

    def print_stats(self):
        stats = self.solution_statistics()
        for p_id in stats:
            print("{}: {} {} {}".format(p_id, stats[p_id][0].steps, stats[p_id][0].solver,
                stats[p_id][0].solver_version))

    def problems_last_timestamps(self):
        ret = {}
        for problem_id in self.problems:
            ret[problem_id] = self.problems[problem_id].last_solution_timestamp()
        return ret

    def get_task_queue(self):
        if self.num_problems_without_solution() > 0:
            problem_ids = self.problems_without_solution()
            ret = [(self.solver_best, problem_id) for problem_id in problem_ids]
            return ret
        
        timestamps = self.problems_last_timestamps()
        ret = []
        while len(timestamps) > 0:
            oldest_problem_id = min(timestamps, key=timestamps.get)
            del timestamps[oldest_problem_id]
            if not (self.solver_best, oldest_problem_id) in ret:
                ret.append((self.solver_best, oldest_problem_id))
        return ret
        #for p_id in self.problems:
        #    for solver in self.solvers:
        #        if (not (solver.name, solver.version) in self.problems[p_id].solvers_tried()) and\
        #                (not solver.solver is None):
        #            return (solver, p_id)

        raise NotImplementedError('Solved all problems. What should we prioritize now?')

    def run(self, num_threads = 1):
        problem_list = self.get_task_queue()
        #print(problem_list)
        #input("Press key...")
        start_submit_wait = int(time.mktime(time.gmtime())) - 1000
        while True:
            threads_active = threading.active_count() - 1
            #print("Manager: Current worker threads active: {}".format(threads_active))
            #input("Press key...")
            for i in range(threads_active, num_threads):
                if len(problem_list) == 0:
                    problem_list = self.get_task_queue()
                (solver, problem_id) = problem_list.pop(0)
                thread = threading.Thread(target=self.run_solver, args=(copy.deepcopy(solver),
                        problem_id), kwargs={'time_budget_per_tile': 0.0005})
                thread.start()
                #self.run_solver(copy.deepcopy(solver), problem_id, time_budget_per_tile=0.0001)
            #input("Press key...")
            time.sleep(1)
            if self.can_submit_solution() and int(time.mktime(time.gmtime())) - start_submit_wait > 300:
                start_submit_wait = int(time.mktime(time.gmtime()))
                with self._lock:
                    print("(Manager) *** Uploading submission ***")
                    print("(Manager) Total steps needed:    {}".format(self.total_steps))
                    print("(Manager) Total steps submitted: {}".format(self.total_steps_submitted))
                    path = self.create_submission_archive()
                    self.upload_submission(path)

    def run_solver(self, solver, problem_id, time_budget=None,
            time_budget_per_tile=None, silent=True):
        if len(self.problems[problem_id].solutions) > 0:
            steps_prev = self.problems[problem_id].best_solution().steps
        else:
            steps_prev = -1
        print("(Problem {}) Running with solver {}, version {}. Previous best solution needed {} steps".format(str(problem_id).zfill(3), solver.name, solver.version,steps_prev))
        time_start = time.perf_counter()
        #input("Press key...")

        queue = multiprocessing.Queue()
        def _solver_run():
            solution = solver.run(problem_id,silent=silent, time_budget=time_budget, time_budget_per_tile=time_budget_per_tile)
            queue.put(solution)
            return solution
        process = multiprocessing.Process(target=_solver_run)
        process.start()
        solution = queue.get()
        
        time_end = time.perf_counter()
        if solution is None:
            raise Exception("There was a problem and a solution could not be found.")
        if self.problems[problem_id].problem_id != problem_id:
            raise Exception("Problem, wrong id?")
        steps_now = solution.steps
        with self._lock:
            if len(self.problems[problem_id].solutions) == 0:
                self.total_steps = self.total_steps + steps_now
                self.newly_solved_problems = True
                #print("newly solved!")
                #input("Press key...")
            elif solution.steps < steps_prev:
                self.total_steps = self.total_steps - steps_prev + steps_now
                print("(Problem {}) --> Improved step count by {}!".format(str(problem_id).zfill(3),steps_prev - steps_now))
            else:
                print("(Problem {}) --> Worse by {} steps compared to the previous record.".format(
                    str(problem_id).zfill(3), steps_now - steps_prev))
            time_needed = time_end - time_start
            #print("--> (Problem {}) Time: {}% - took {}s out of the alloted {}s".\
            #        format(str(problem_id).zfill(3), time_needed))
            print("(Problem {}) --> Time: {}s".\
                    format(str(problem_id).zfill(3), time_needed))
            self.problems[problem_id].solutions.append(solution)
            #self.print()
            self.save_manager_configuration()


    def upgrade_solutions(self):
        for problem_id in self.problems:
            print("Updating solutions for problem {}".format(problem_id))
            for solution in self.problems[problem_id].solutions:
                solution.upgrade_on_disk()

    def create_submission_archive(self):
        our_steps = {}
        tmpfolder = pathlib.Path(base_path_tmp)
        if tmpfolder.exists():
            raise Exception('Folder {} should not exist!'.format(tmpfolder))
        tmpfolder.mkdir()
        print("Creating submission archive")
        timestamp = int(time.mktime(time.gmtime()))
        total_steps = 0
        for problem in [self.problems[p_id] for p_id in self.problems if \
                len(self.problems[p_id].solutions)>0]:
            best_solution = problem.best_solution()
            #print("Best known solution for problem {} is {} and needs {} steps".format(problem.problem_id, best_solution.path(), best_solution.steps))
            #print(best_solution)
            target_file = tmpfolder / best_solution.spec_filename()
            #print(target_file)
            #input("Press key...")
            with target_file.open('w') as f:
                f.write(best_solution.solution)
            total_steps = total_steps + best_solution.steps
            our_steps[str(problem.problem_id)] = best_solution.steps

        self.total_steps = total_steps
        archive_path = pathlib.Path(base_path_submissions) / \
                (str(timestamp) +  '-' + str(total_steps) + '.zip')
        os.system('cd ' + base_path_tmp + ' && zip --quiet -r ' + archive_path.as_posix() + ' .')

        for solution in tmpfolder.iterdir():
            #print("Deleting {}".format(solution))
            solution.unlink()
        tmpfolder.rmdir()
        
        with archive_path.open('rb') as f:
            archive_content = f.read()
            archive_hash = hashlib.sha256(archive_content).hexdigest()

        self.solution_archives[archive_hash] = our_steps

        self.save_manager_configuration()
        return archive_path

    def can_submit_solution(self):
        return int(time.mktime(time.gmtime())) - \
            self.last_solution_submission_timestamp > 700
        # some extra seconds as a safety buffer it the time is counted
        # differently (e.g. only after our solution has been verified)

    def get_last_submission_stats(self, our_steps=None):
        #print(self.solution_archives)
        url = 'https://monadic-lab.org//grades/{}/submissions.txt'.format(self.api_id)
        response = requests.get(url)
        if response.status_code != requests.codes.ok:
            raise Exception("Could not get uploaded submissions information!")
        data = [line for line in response.text.split(sep='\n') if line != '']
        last_line = data[-1].split(sep=' ')
        last_submission = last_line[0]
        last_hash = last_line[2]
        #print("Last submission: {}, hash: {}".format(last_submission,last_hash))

        url = 'https://monadic-lab.org/grades/{}/{}/score.csv'.format(self.api_id, last_submission)
        response = requests.get(url)
        if response.status_code != requests.codes.ok:
            raise Exception("Could not get uploaded submissions information!")
        data = response.text.split(sep='\n')
        for line in data:
            line = line.split(sep=', ')
            problem_id = int(line[0])
            steps = int(line[1])
            if line[2] != 'Ok':
                raise Exception("There is a submission that failed,\
but there should not be: problem {}".format(problem_id))
            if steps - self.problems[problem_id].best_solution().steps > 0:
                color = BACKGROUND_COLOR_RED
            elif steps - self.problems[problem_id].best_solution().steps < 0:
                color = BACKGROUND_COLOR_GREEN
            else:
                color = BACKGROUND_COLOR_DEFAULT
            print("{} Problem {}: Our steps: {} Steps really {} Difference: {} {}".format(
                color,
                str(problem_id).zfill(3),
                self.problems[problem_id].best_solution().steps,
                steps,
                steps - self.problems[problem_id].best_solution().steps,
                BACKGROUND_COLOR_DEFAULT))
       #self.last_submission_best_team_steps
#
    def check_for_wrong_counts(self):
        self.get_last_submission_stats()

    def upload_submission(self, path, force=False):
        total_steps = int(path.name.split(sep='-')[1].split(sep='.')[0])
        if not self.can_submit_solution():
            print("Too early to submit a new solution!")
            return -1
        if (not force) and (not self.newly_solved_problems) and \
                hasattr(self,'total_steps_submitted') and\
                self.total_steps_submitted <= total_steps:
            print("No reason to upload, already did upload a better or equal solution")
            return -2
        print("Uploading submission {}".format(path))
        url = 'https://monadic-lab.org/submit'
        data = {'private_id': self.api_id}
        files = {'file': path.open('rb')}
        response = requests.post(url,data=data, files=files)
        #print("Got response:")
        #print(response.text)
        #print("Status code was {}".format(response.status_code))
        if response.status_code != requests.codes.ok:
            raise Exception("Upload failed!")
        self.last_solution_submission_timestamp = int(time.mktime(time.gmtime()))
        self.total_steps_submitted = total_steps
        self.newly_solved_problems = False
        self.save_manager_configuration()

    # dict that for each problem_id is a tuple of the best solution
    # and a dict where (name, version) -> difference from the solvers
    # best solution to the best solution overall (non-negative, except
    # -1 for when no solution exists)
    def solution_statistics(self):
        stats = {}
        for problem in [self.problems[p_id] for p_id in self.problems if \
                len(self.problems[p_id].solutions)>0]:
            best_solution = problem.best_solution()
            solver_stats = {}
            for solver in self.solvers:
                solver_best =problem.best_solution_solver(solver.name, solver.version)
                if solver_best is None:
                    solver_stats[(solver.name, solver.version)] = \
                            -1
                else:
                    solver_stats[(solver.name, solver.version)] = \
                            solver_best.steps - best_solution.steps
            stats[problem.problem_id]=(best_solution,solver_stats)
        return stats

def profile_test():
    manager = Manager()
    problem_id = 99
    time_budget = None
    manager.run_solver(
            Solver(solver=delta_solver1,version=delta_solver_version,name='delta'),
            problem_id, time_budget=time_budget, silent=False)


def main():
    if len(sys.argv) < 2:
        print("""auto NUM_PROCESSES
upload-submission
solve NUM
info
stats
check-counts
visualize""")
        return
    if sys.argv[1] == 'auto':
        manager = Manager()
        if len(sys.argv) < 3:
            raise Exception("Missing thread number as argument")
        manager.run(num_threads = int(sys.argv[2]))
    elif sys.argv[1] == 'pack-submission':
        manager = Manager()
        manager.print()
        manager.create_submission_archive()
    elif sys.argv[1] == 'upload-submission':
        force = False
        if len(sys.argv) > 2 and sys.argv[2] == 'force':
            force = True
        manager = Manager()
        path = manager.create_submission_archive()
        manager.upload_submission(path, force=force)
    elif sys.argv[1] == 'solve':
        if len(sys.argv) < 3:
            raise Exception('Missing argument: problem id')
        manager = Manager()
        manager.print()
        problem_id = int(sys.argv[2])
        if len(sys.argv) > 3:
            time_budget = int(sys.argv[3])
        else:
            time_budget = None
        manager.run_solver(
                Solver(solver=delta_solver1,version=delta_solver_version,name='delta'),
                problem_id, time_budget=time_budget, silent=False)
    elif sys.argv[1] == 'info':
        manager = Manager()
        manager.print()
    elif sys.argv[1] == 'stats':
        manager = Manager()
        manager.print_stats()
    elif sys.argv[1] == 'upgrade-solutions':
        manager = Manager()
        manager.upgrade_solutions()
    elif sys.argv[1] == 'check-counts':
        manager = Manager()
        manager.check_for_wrong_counts()
    elif sys.argv[1] == 'visualize':
        if len(sys.argv) < 4:
            raise Exception("Missing argument: problem file name and solution file name")
        problem = sys.argv[2]
        solution_path = pathlib.Path(sys.argv[3])
        solution = RichSolution()
        solution.read_from_disk(solution_path)
        situation = Situation(task_filename = problem)
        situation.print_situation()
        input("Press key...")
        jump_ahead_manip = False
        cmd = ''
        for command in solution.solution:
            if command == 'B':
                jump_ahead_manip = True
            if jump_ahead_manip:
                cmd += command
                if command == ')':
                    jump_ahead_manip = False
                    command = 'B'
                    cmd = ''
                else:
                    continue
            delta = SituationDelta(parent_situation=situation, command=command)
            situation.apply_delta(delta)
            situation.print_situation()
            print("Command was: {}".format(command))
            input("Press key...")
    elif sys.argv[1] == 'dev':
        #m = manipulator_vectors_for_number_direction(10,DIRECTION_DOWN)
        #print(manipulator_vectors_for_number_direction(10,DIRECTION_RIGHT))
        #cProfile.run('solver_wrapper(path_solver, 40)')
        #tracemalloc.start()
        #Solver(solver=path_solver3,version=3,name='path').run(70)
        #snapshot = tracemalloc.take_snapshot()
        #top_stats = snapshot.statistics('lineno')
        #print("[ Top 10 ]")
        #for stat in top_stats[:10]:
        #        #print(stat)
        #
        #cProfile.run('profile_test()')
        #print(manager.problems_last_timestamps())
        #manager.print()
        #manager.create_submission_archive()
        #path = pathlib.Path('/home/user/solutions/prob-039/1029-00000')
        #solution = RichSolution()
        #solution.read_from_disk(path)
        #print(solution.__dict__)
        #path = pathlib.Path('/home/user/solutions/prob-039/1063-00000')
        #solution = RichSolution()
        #solution.read_from_disk(path)
        #print(solution.__dict__)
        #print(Line(Point(2,0),Point(0,0)).orientation)
        situation = Situation(task_filename='/home/user/problems/prob-295.desc')
        #input("Press key...")
        #situation.print_situation()
        #delta = SituationDelta(situation)
        #print(delta.bot_location)
        #print(delta.can_move_in_direction(DIRECTION_RIGHT))
        #delta.apply_command(DIRECTION_UP)
        #delta.print_changes()
        #delta2 = SituationDelta(situation,command=DIRECTION_UP)
        #delta2.print_changes()
        #delta.print_changes()
        #print(delta == delta2)
        #s = set()
        #s.add(delta)
        #s.add(delta2)
        #print(s)
        #situation.apply_delta(delta)
        #input("Press key...")
        #situation.print_situation()
        #print(situation.bounding_polygon)
        #poly = situation.bounding_polygon
        #poly._generate_intersection_hash()
        #print(poly._horizontal)
        #print(poly._vertical)
        #print(Line(Point(3,5),Point(3,17)).intersect(Line(Point(2,7),Point(10,7))))
        #print(poly.intersect(Line(Point(3,5),Point(3,17))))
        #print(poly.intersect(Line(Point(27,5),Point(30,5))))
        #situation.print_situation(path_mode=True)
        #print(situation.paths(max_length=4, sorted_by_distance=True))
        #print(situation.paths(max_length=4, sorted_by_distance=False))
        pass
    else:
        print("Unknown command!")

if __name__ == '__main__':
    main()

